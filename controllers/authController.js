const User = require('./../models/userModels')
const jwt  = require('jsonwebtoken')
const {promisify} = require('util')
const AppError = require('./../utils/appError')

const signToken=(id)=>{
    return jwt.sign({id},process.env.JWT_SECRET,{
        expiresIn: process.env.JWT_EXPIRES_IN,
    })
}

const creatSendToken = (user,statusCode,res)=>{
    const token = signToken(user._id)
    const cookieOptions = {
        expires:new Date(
            Date.now()+process.env.JWT_COOKIE_EXPIRES_IN *24*60*60*1000,
        ),
        httpOnly:true,
    }
    res.cookie('jwt',token,cookieOptions)
    res.status(statusCode).json({
        status:'success',
        token,
        data:{
            user
        } 
    })    
}
exports.signup = async(req,res,next)=>{
    try{
    const newUser = await User.create(req.body)
    creatSendToken(newUser,201,res)
    }catch(err){
        res.status(500).json({error:err.message});
    }
}

exports.login = async (req,res,next)=>{
    try{
        // console.log(req.headers)
        const {email,password} = req.body
        // console.log(email,password)

        if(!email||!password){
            return next(new AppError('please provide an email and password!',400))
        }

        const user = await User.findOne({email}).select('+password')
        
        if(!user||!(await user.correctPassword(password,user.password))){
            return next(new AppError('Incorrect email or password', 401))
        }

        creatSendToken(user,200,res)
        // const token = signToken(user._id)
        // res.status(200).json({
        //     status : 'success',
        //     token,
        // })

    }catch(err){
        res.status(500).json({error:err.message});

    }
}
exports.protect = async(req,res,next) =>{
    try{
        // 1) Getting token and check of it's there
        let token
        if(req.headers.authorization &&
            req.headers.authorization.startsWith('Bearer')
        ){
            token = req.headers.authorization.split(' ')[1]
        }
        else if(req.cookies.jwt){
            token =  req.cookies.jwt
        }
        if(!token){
            return next(
                new AppError('you are not logged in! Please login in to get access.',401),
            )
        }
        // 2) Verification token
        const decoded = await promisify(jwt.verify)(token,process.env.JWT_SECRET)
        console.log(decoded)
        
        // 3) Check if user still exists
        const freshUser = await User.findById(decoded.id)
        if(!freshUser){
            return next(
                new AppError('The user belonging to this token no longer exist',401),
            )
        }
        // Grant access to protected route
        req.user = freshUser;
        next()
    }
    catch(err){
        res.status(500).json({error:err.message});
    }
}

exports.updatePassword = async(req,res,next)=>{
    try{
        const user  = await User.findById(req.user.id).select('+password')
        if(!(await user.correctPassword(req.body.passwordCurrent,user.password))){
            return next(new AppError('Your current password is wrong',401))
        }
        user.password = req.body.password
        user.passwordConfirm = req.body.passwordConfirm
        await user.save()
        
        creatSendToken(user,200,res)
    }
    catch(err){
        res.status(500).json({error:err.message});
    }
}

exports.restrictTo = (...roles) => {
    return (req, res, next) => {
      // roles ['admin', 'lead-guide']. role='user'
      if (!roles.includes(req.user.role)) {
        return next(
          new AppError('You do not have permission to perform this action', 403)
        )
      }
      next()
    }
}  


exports.logout = (req,res)=>{
    res.cookie('token','',{
        expires: new Date(Date.now()+10*1000),
        httpOnly:true,
    })
    res.status(200).json({status:'success'})
}





